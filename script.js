let grades = [98, 94, 89, 90];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway","Toshiba", "Fujitsu"];

//code
console.log(grades[0]);
console.log(computerBrands[3]);

let myTask = ["bake sass", "drink html", "inhale css", "eat JavaScript"];

console.log(myTask);

myTask[0] = "hello world!"
console.log(myTask);

//Getting the length of an array.

console.log(computerBrands.length);

let lastIndex = myTask.length -1;

//array methods
//1. mutator method

//push function

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Current array");
console.log(fruits);
console.log("Array after using push");

let fruitsLength = fruits.push("Strawberry");

console.log(fruits);
console.log(fruitsLength);

fruits.push("Guava", "Avocado");
console.log("Mutated array after push();");
console.log(fruits);

//pop function

let fruitRemoved = fruits.pop()
console.log("Mutated array after pop();");
console.log(fruits);

//shift function
let firstElement = fruits.shift();
console.log("Mutated array after shift();");
console.log(fruits);
console.log(firstElement);

//unshift function
let firstAddedElement = fruits.unshift("Banana");
console.log("Mutated array after shift();");
console.log(fruits);
console.log(firstAddedElement);
;
//splice function
let fruitsSplice = fruits.splice (1, 2, "Lime", "Cherry");
console.log(fruitsSplice);
console.log(fruits);

//sort function
fruits.sort();
console.log("Mutated array after sort();");
console.log(fruits);

//reverse function
fruits.reverse();
console.log("Mutated array after reverse();");
console.log(fruits);

//Non mutator methods
// 
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

//indexof function
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf() method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf() method;" + invalidCountry);

//lastindexOf() function
let lastIndexStart = countries.lastIndexOf("PH", 7);
console.log("Result of lastIndexOf() method: " + lastIndexStart);

//slice function
let slicedArrayA = countries.slice(2);
console.log("Result from slice method A" + slicedArrayA);
let slicedArrayB = countries.slice(2, 4);
console.log("result from slice method B:" + slicedArrayB)
let slicedArrayC = countries.slice(-3);
console.log("result from slice method C:" + slicedArrayC)


// toStrings();

let stringArray = countries.toString();
console.log("Result from toString()");
console.log(stringArray);

//concatenate function

let tasksArrayA = ["drink HTML", "eat Javascript"];
let tasksArrayB = ["inhale CSS", "breathe SASS"];
let tasksArrayC = ["get git", "be node"];

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("Result from concat()");
console.log(allTasks);

let combineTasks = tasksArrayA.concat("smell express", "small express");

console.log(combineTasks);

//join function
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));

//Iteration Method
// for each function

countries.forEach(function(country){
	console.log(country);
})

allTasks.forEach(function(task){
	console.log(task);
})


let filteredTasks = [];
allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})

console.log("Result from forEach()");
console.log(filteredTasks);

//map function

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
})

console.log("Result from map()");
console.log(numberMap);

let arrayMap = allTasks.map(function(task){
	return `My ${task}`;
})

console.log(arrayMap);

//every function

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("Result from every()");
console.log(allValid);

//some function

let someValid = numbers.some(function(number){
	return (number < 3);
})

console.log("Result from some()");
console.log(someValid);

//filter function

let filterNumbers = numbers.filter(function(number){
	return (number < 3);
})

console.log("Result from some()");
console.log(filterNumbers);

//includes function

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let filterProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})

console.log("Result from filter and include()");
console.log(filterProducts);

// multi dimensional array


let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoar[1][4]);
console.log("Pawn moves to f2: " + chessboard[1][5]);
console.log("Knight moves to c7: " + chessboard[6][2]);